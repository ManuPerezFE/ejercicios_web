
function calculoNumeroAleatorio(){
    
    let num1 = crearNumAleatorio();
    let num2 = crearNumAleatorio();
    let num3 = crearNumAleatorio();
    if(num1 == num2 || num1 == num3)
    {
        num1 = crearNumAleatorio();
    }
    if( num2 == num1 || num2 == num3){
        num2 = crearNumAleatorio();
    }
    if(num3 == num1 || num3 == num2){
        num3 = crearNumAleatorio();
    }
    document.getElementById("cirulo1").innerHTML = num1;
    document.getElementById("cirulo2").innerHTML = num2;
    document.getElementById("cirulo3").innerHTML = num3;
}

function crearNumAleatorio(){
    let numAleatorio = Math.round(Math.random()*50);
    return numAleatorio;
}

function numeroAleatorio(min, max) {
    return Math.round(Math.random() * (max - min) + min);
  }
function piedraPapelTijera(opcion){
    //recibo un valor
    //0-piedra 1-papel 2-tijera
    opcion = parseInt(opcion);//lo paso a int
    numeroIA = numeroAleatorio(0,2);//genero un numero aleatorio (0-1-2)
    //voy mirando que opcion me han dado y la comparo con el numero que he generado
    if(opcion == 0){//piedra
        if(numeroIA == 0){//piedra
            document.getElementById("resultado").innerHTML = "Empate!"; 
        }else if(numeroIA == 1){//papel
            document.getElementById("resultado").innerHTML = "Has perdido!"; 
        }else{//tijera
            document.getElementById("resultado").innerHTML = "Has ganado!"; 
        }
    }
    if(opcion == 1){//papel
        if(numeroIA == 0){ //piedra
            document.getElementById("resultado").innerHTML = "Has ganado!"; 
        }else if(numeroIA == 1){//papel
            document.getElementById("resultado").innerHTML = "Empate!"; 
        }else{//tijera
            document.getElementById("resultado").innerHTML = "Has perdido!"; 
        }
    }
    if(opcion == 2){//tijera
        if(numeroIA == 0){ //piedra
            document.getElementById("resultado").innerHTML = "Has perdido!"; 
        }else if(numeroIA == 1){//papel
            document.getElementById("resultado").innerHTML = "Has ganado!"; 
        }else{//tijera
            document.getElementById("resultado").innerHTML = "Empate!"; 
        }
    }
    opciones = ["piedra", "papel", "tijera"];//para ver la opcion
    console.log("Opcion seleccionada: " + opciones[opcion]);
    console.log("Opcion IA: " + opciones[numeroIA]);
}