
class Figura{
    constructor(x,y){
        this.x = x;
        this.y = y;
    }
}

class Rectangulo extends Figura{
    constructor(x, y, lado1, lado2){
        super(x,y);
        this._lado1 = lado1;
        this._lado2 = lado2;
    }

    getArea(){
        return this.lado1 * this.lado2;
    }
}

class Triangulo extends Figura{
    constructor(x, y, base, altura){
        super(x,y);
        this.base = base;
        this.altura = altura;
    }

    getArea(){
        return (this.base * this.altura) / 2;
    }
}

class Cuadrado extends Rectangulo{
    constructor(x,y,lado){
        super(x,y,lado,lado);
    }
}