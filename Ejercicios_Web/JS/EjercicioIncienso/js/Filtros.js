
const MAIL_REGEX = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

class Filtros{

    mensajeError = "";

    filtraCorreo(correo){
        if(!MAIL_REGEX.test(correo.value)){
            this.comunicaError(correo.value + " - formato de correo erroneo");
        }
    }

    comunicaError(mensaje){
        this.mensajeError += "<li>" + mensaje + "</li>";
    }
    presentaError(){
        let salida = "<ul>" + this.mensajeError + "</ul>";
        this.mensajeError = "";
        return salida;
    }
}