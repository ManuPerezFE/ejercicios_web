
function obtenerMayor(){
    num1Mayor = document.getElementById("num1Mayor").value;
    num2Mayor = document.getElementById("num2Mayor").value;
    mayor(num1Mayor,num2Mayor);
}
function mayor(a,b){
    if (a>b){
        return document.getElementById("ejNumMayor").innerHTML = "El numero " + a + " es mayor.";
    }else if(b>a){
        return document.getElementById("ejNumMayor").innerHTML = "El numero " + b + " es mayor.";
    }else{
        return document.getElementById("ejNumMayor").innerHTML = "Son iguales.";
    }
}

function obtenerDatos(){
    numDatos = document.getElementById("numDatos").value;
    datos(numDatos);
}
function datos(x){
    let res = "";
    if(x%2 == 0){
        res += x + " es par. ";
    }else{
        res += x + " no es par. ";
    }

    if(x%3 == 0){
        res += x + " es divisible por 3. ";
    }else{
        res += x + " no es divisible por 3. ";
    }

    if(x%5 == 0){
        res += x + " es divisible por 5. ";
    }else{
        res += x + " no es divisible por 5. ";
    }
    if(x%7 == 0){
        res += x + " es divisible por 7. ";
    }else{
        res += x + " no es divisible por 7. ";
    }
    return document.getElementById("ejDatos").innerHTML = res;
}
function obtenerSumaValores(){
    numeros = document.getElementById("numSumaValores").value;
    arr = numeros.split(',');
    sumaValores(arr);
}
function sumaValores(arr){
    let suma = 0;
    for(valor in arr){
        suma += parseFloat(arr[valor]);
    }
    return document.getElementById("ejSumaValores").innerHTML = "Ejercicio 3: La suma es: " + suma;
}

function obtenerFactorial(){
    numFactorial = document.getElementById("numFactorial").value;
    factorial(numFactorial);
}
function factorial(x){
    let fact = 1; 
    for (let i=1; i<=x; i++) {
        fact *= i; 
    }
    return document.getElementById("ejFactorial").innerHTML = "El factorial de " + x + " es : " + fact;
}

function obtenerPrimo(){
    numPrimo = document.getElementById("numPrimo").value;
    primo(numPrimo);
}
function primo(primo){
    for(let a=primo-1; a>1;a--){
        if(!(primo%a))return document.getElementById("ejNumPrimo").innerHTML = "Ejercicio 5: El numero " + primo + " no es primo";
    }
    return document.getElementById("ejNumPrimo").innerHTML = "Ejercicio 5: El numero " + primo + " no es primo";
}

function obtenerFibonacci(){
    numFibonacci = document.getElementById("numFibonacci").value;
    fibonacci(numFibonacci);
}
function fibonacci(n){
    n = parseFloat(n);
    let arr = [0, 1];
    for (let i = 2; i < n + 1; i++){
        arr.push(arr[i - 2] + arr[i -1])
    }
    return document.getElementById("ejNumFibonacci").innerHTML = "Ejercicio 6: " + arr;
}

function obtenerPrimoCifras(){
    numPrimoCifras = document.getElementById("numPrimoCifras").value;
    primoCifras(numPrimoCifras);
}
function primoCifras(x){
    let busca = true;
    let inicio = Math.pow(10, x-1);
    while(busca){
        if(primo(inicio)){
            return document.getElementById("ejPrimoCifras").innerHTML = "Ejercicio 7: El primer numero primo de " + x + " cifras es: " + inicio;
        }
        inicio++;
    }
}

function obtenerCapitaliza(){
    ciudadCapitaliza = document.getElementById("ciudadCapitaliza").value;
    capitaliza(ciudadCapitaliza);
}
function capitaliza(x){
    min = x.toLowerCase();
    may = x.toUpperCase();
    may = may.substring(0, 1);
    min = min.substring(1, min.length);
    return document.getElementById("ejCiudadCapitalizada").innerHTML = "Ejercicio 8: " + may + min;
}

function obtenerPalabra(){
    ciudadPalabra = document.getElementById("ciudadPalabra").value;
    palabra(ciudadPalabra);
}
function palabra(c){
    let res = "";
    c = c.toLowerCase();
    res = '"' + c + '" tiene ' + c.length + ' letras.\n';
    if(c.length%2 == 0){
        res += c.length + " es par.\n";
    }else{
        res += c.length + " no es par.\n";
    }
    let contVocales = 0;
    let contConsonantes = 0;
    for(let i=0; i<c.length;i++){
        if(c[i] == "a" || c[i] == "e" || c[i] == "i" || c[i] == "o"|| c[i] == "u"){
            contVocales++;
        }else{
            contConsonantes++;
        }
    }
    res += "Tiene " + contVocales + " vocales.\n";
    res += "Tiene " + contConsonantes + " consonantes.\n";

    return document.getElementById("ejCiudadPalabra").innerHTML = "Ejercicio 9: " + res;
}

function hoy(){
    let dia = new Date();
    let dias = new Array ("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
    return dias[dia.getDay()];
}
document.getElementById("ejHoy").innerHTML = "Ejercicio 10: Hoy es " + hoy();

function navidad(){
    let fechaInicio = new Date().getTime();
    let fechaFin = new Date('2019-12-25').getTime();

    var diff = fechaFin - fechaInicio;

    return Math.round(diff/(1000*60*60*24));
}
document.getElementById("ejNavidad").innerHTML = "Ejercicio 11: Faltan " + navidad() + " dias para navidad.";

function obtenerAnaliza(){
    numAnaliza = document.getElementById("numAnaliza").value;
    arrNumAnaliza = numAnaliza.split(',');
    analiza(arrNumAnaliza);
}
function analiza(x){
    let cantidad = x.length;
    let res = "";
    let suma = 0;
    let numMayor = 0;
    let numMenor = 999999999;
    for(let i = 0; i<cantidad;i++){
        suma += parseFloat(x[i]);
        if(numMayor<x[i]){
            numMayor = x[i];
        }
        if(numMenor>x[i]){
            numMenor = x[i];
        }
    }
    res += "La suma de los " + cantidad + " digitos es: " + suma + "\n";
    res += "El numero mayor es: " + numMayor + "\n";
    res += "El numero menor es: " + numMenor + "\n";

    return document.getElementById("ejNumAnaliza").innerHTML = "Ejercicio 12: " + res;
}

let numeroAleatorio = Math.round(Math.random()*10); 
let intentos = 0;
let res = "";
let adivinado = true;
while(adivinado){
    let numeroAdivinar = prompt("Escribe un numero entre el 0 y el 10 (entero)", 5);
    if(numeroAdivinar == numeroAleatorio){
        intentos++;
        alert("BIEN! adivinado!");
        alert("Numero de intentos " + intentos);
        adivinado = false;
    }else if(numeroAdivinar > numeroAleatorio){
        intentos++;
        alert("Demasiado alto, vuelve a intentar");
    }else if(numeroAdivinar < numeroAleatorio){
        intentos++;
        alert("Demasiado bajo, vuelve a intentar");
    }
}
