
$.getJSON(
    "https://api.citybik.es/v2/networks/bicing",
    function (data) {
        var estacions = data.network.stations;
        console.log(estacions);

        estacionVacia = 0;
        for(let i = 0; i < estacions.length; i++){
            estacion = estacions[i];
            if(estacionVacia <= estacion.free_bikes){
                estacionVacia = estacion.free_bikes;
            }
        }
        
        estacionMenosBicis = estacions.filter(estacion => estacion.free_bikes == estacionVacia)
        a = estacions.sort(function (a, b) {
            if (a.free_bikes > b.free_bikes) {
              return 1;
            }
            if (a.free_bikes < b.free_bikes) {
              return -1;
            }
            return 0;
          });
        
        b = estacions.sort(function(a,b){return a.free_bikes - b.free_bikes;});
        
        console.log("La estación con más bicis libres es:");
        console.log(a[0].name);
        console.log(b[0].name);
        console.log(estacionMenosBicis[0].name);
        //ESTACIONES SIN BICIS
        estacionesSinBicis = estacions.filter(estacion => estacion.free_bikes == 0);
        console.log("Las estaciones siguientes no tienen bicis:");
        for(let j = 0; j <estacionesSinBicis.length; j++){
            console.log(estacionesSinBicis[j].name);
        }
        
});