const PRODUCTOS = 
[
    {
      "id_producto": "1",
      "pro_descripcion": "Descripcion 1",
      "pro_desLarga": "Des Larga 1",
      "pro_precio": 1,
      "pro_stock": 1,
      "pro_fecRepos": "01/07/2019",
      "pro_fecActi": "01/07/2019",
      "pro_fecDesacti": "01/07/2019",
      "pro_uniVenta": "1",
      "pro_cantXUniVenta": 1,
      "pro_uniUltNivel": "1",
      "id_pais": 1,
      "pro_usoRecomendado": "Uso recomendado 1",
      "id_categoria": 1,
      "pro_stkReservado": 1,
      "pro_nStkAlto": 1,
      "pro_nStkBajo": 1,
      "pro_stat": "a"
    }
];

const USUARIOS = 
[
    {
      "id_usuario": 1,
      "user_nombre": "Nombre 1",
      "user_email": "mail1@gmail.com",
      "user_pass": "contraseña1",
      "user_tipo": 1,
      "user_dni": "41005847k",
      "user_fecAlta": "01/07/2019",
      "user_fecConfirmacion": "01/07/2019",
      "user_datosPago": "datos pago 1",
      "user_datosEnvio": "datos envio 1"
    }
]

const CATEGORIA = 
[
    {
      "id_categoria": 1,
      "cat_nombre": "Categoria 1",
      "cat_descripcion": "Descripcion 1"
    }
]

const DIRECCIONES = 
[
    {
      "dir_nombre": "Manu1",
      "dir_direccion": "Direccion 1",
      "dir_poblacion": "Poblacion 1",
      "dir_cPostal": "cPostal 1",
      "dir_provincia": "Provincia 1",
      "dir_pais": "Pais 1",
      "dir_correoE": "mail@gmail.com"
    }
]

const CARRITO = 
[
    {
      "id_pedido": 1,
      "id_usuario": 1,
      "id_producto": "1",
      "car_cantidad": 1,
      "car_precio": 1,
      "car_envio": "Direccion 1",
      "car_pago": "Direccion 1",
      "car_tarjeta": "1111111111111111111111",
      "car_feCadud": "01/07/2019",
      "car_ccv": 1,
      "car_nombre": "Manu 1",
      "car_stat": 1,
      "car_feCambio": ["01/07/2019", "01/07/2019"]
    }
];