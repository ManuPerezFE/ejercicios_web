const mysql = require('mysql');
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'manu',
    password: 'manu',
    database: 'tienda'
});

connection.connect((err) => {
    if (err) throw err;
    console.log('Conectado!');
});

connection.query('SELECT * FROM c_pais',
    (err, rows) => {
        if(err) throw err;
        console.log('Data receibed from DB:\n');
        //console.log(rows);
        rows.forEach(row => {
            console.log(`${row.country_id} ${row.short_name}`);
        });
    }
);

const cate = {
    id_categoria: 0,
    cat_nombre: 'Incienso',
    cat_descripcion: 'Incienso cuidadosamente fabricado'
};

connection.query('INSERT INTO categorias SET ?', cate, (err, res) => {
    if(err) throw err;
    console.log('Last insert ID:', res.insertId);
});