const http = require('http');
const qs = require('querystring');
const func = require('./Conversiones');

const hostname = "localhost";
const port = 3000;

const informe = {
    celsius: 0,
    faren: ""
}

var formulario = `<form method="POST" action="/url">
<label>Temperatura (C)</label>
<input type="number" name="centigrados" value={{1}} /> {{2}}
<input type="submit" value="Enviar" />
</form>`;

const server = http.createServer((req, res) =>{
    if(req.method == 'GET'){
        dialogoGET(req, res)
    }else{
        if(req.method == 'POST'){
            dialogoPOST(req, res);
        }else{
            console.log(req.headers);
            res.statusCode = 500;
            res.setHeader('Content-Type', 'text/html');
            res.end('Metodo desconocidos=' + req.method);
        }
    }
});

server.listen(port, hostname, () =>{
    console.log(`Server running at http://${hostname}:${port}/`);
});

function dialogoGET(req, res){
    if('/' == req.url){
        enviarFormulario(res, informe);
    }
}

function dialogoPOST(req, res){
    console.log('POST reconocido');
    var body = '';
    req.on('data', function(trozo){
        body += trozo;
    });

    req.on('end', function(){
        console.log(body);
        if('/url' == req.url){
            console.log("Url reconocido");
            informe.celsius = qs.parse(body).centigrados;
            informe.faren = 'Convertido a F, es: ' + func.c2f(informe.celsius);

            enviarFormulario(res, informe);
        }
    })
}



function enviarFormulario(res, informe){
    console.log(informe);
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    salida = formulario;
    salida = salida.replace("{{1}}", informe.celsius);
    salida = salida.replace("{{2}}", informe.faren);

    res.end(salida);
}