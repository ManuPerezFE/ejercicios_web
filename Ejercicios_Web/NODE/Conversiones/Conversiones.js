module.exports = {
  m2km: function(m) {
    return m * 1.609344;
  },
  km2m: function(km) {
    return km / 1.609344;
  },
  c2f: function(c) {
      let cToF = c*9/5+32;
      return cToF;
  },
  f2c: function(f) {
      let fToC = (f - 32) * 5 / 9;
      return fToC;
  }
};
