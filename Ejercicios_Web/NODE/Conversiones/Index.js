const func = require('./Conversiones');

let c = 30;
console.log(c + ' ºC. son ' + func.c2f(c));
let f = 86;
console.log(f + " ºF. son " + func.f2c(f));
let m = 1609;
console.log(m + " millas son " + func.m2km(m));
let km = 1000;
console.log(km + " kilometros son " + func.km2m(km));