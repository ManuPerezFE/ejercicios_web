import React, {Component} from 'react';

import InformacionClima from './components/InformacionClima';
import FormularioClima from './components/FormularioClima';

import {WEATHER_APIKEY} from './keys';

class App extends Component{

    constructor(props){
        super(props);
        this.state = { 
            temperatura: '',
            descripcion: '',
            humedad: '',
            viento: '',
            ciudad: '',
            estado: '',
            icon: '',
            error: null
        };
    }
    getCoordenadas(){
        //consigo la geolocalizacion
        return new Promise(function(resolve, reject){
            navigator.geolocation.getCurrentPosition(resolve, reject);
        });
    }
    getDatos = async e => {
        e.preventDefault();
        const {city, country} = e.target.elements;
        const cityValue = city.value;
        const countryValue = country.value;
        
        let WEATHER_FUNCION = "weather";
        let WEATHER_APIURL, respuesta, datos, long, lat;

        if(cityValue == '' && countryValue == ''){
            if(navigator.geolocation){
                const pos = await this.getCoordenadas();
                long = pos.coords.longitude;
                lat = pos.coords.latitude;
                console.log(pos);
            }
            //await this.getCoordenadas();
            WEATHER_APIURL = `http://api.openweathermap.org/data/2.5/${WEATHER_FUNCION}?lat=${lat}&lon=${long}&APPID=${WEATHER_APIKEY}&units=metric`;
        }else{
            WEATHER_APIURL = `http://api.openweathermap.org/data/2.5/${WEATHER_FUNCION}?q=${cityValue},
                ${countryValue}&APPID=${WEATHER_APIKEY}&units=metric`;
            
        }

        respuesta = await fetch(WEATHER_APIURL);
        datos = await respuesta.json();
        console.log(datos);
        
        this.setState({
            temperatura: datos.main.temp,
            descripcion: datos.weather[0].description,
            humedad: datos.main.humidity,
            viento: datos.wind.speed,
            ciudad: datos.name,
            estado: datos.sys.country,
            icon: datos.weather[0].icon,
            error: null
        })
    }
    render(){
        return(
            <div className="container p-4">
                <div className="row">
                    <div className="col-md-6">
                        <FormularioClima origen={this.getDatos} />
                        <InformacionClima {...this.state} />
                    </div>
                </div>
            </div>
        )
    }
}

export default App;