import React, {Component} from 'react';

import Thumbs from './components/Thumbs/Thumbs';
import Tricolor from './components/Tricolor/Tricolor';
import Fotos from './components/Fotos/Fotos';
import ListaCiudades from './components/ListaCiudades/ListaCiudades';
import ListaCombo from './components/ListaCiudades/ListaCombo';
import ListaCiudadesFetch from './components/ListaCiudades/ListaCiudadesFetch';
import Tiempo from './components/Tiempo/Tiempo';

export default class App extends Component {

  render(){
    return(
      <div className="App">
        <Tiempo />
        <Thumbs />
        <Tricolor />
        <Fotos />
        <ListaCiudades />
        <ListaCombo />
        <ListaCiudadesFetch />
      </div>
    )
  }
}
