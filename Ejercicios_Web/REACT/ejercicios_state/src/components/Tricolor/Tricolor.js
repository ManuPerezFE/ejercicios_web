import React, {Component} from 'react';
import './css/Tricolor.css';

export default class Tricolor extends Component{

    constructor(props){
        super(props);
        this.state = { 
            color: 0
        };
        this.cambioColor = this.cambioColor.bind(this);
    }
    cambioColor(){
        this.state.color++;
        this.setState({color: this.state.color});
        if(this.state.color == 4){
            this.setState({color: 0});
        }
    }

    render(){
        return(
            <div id="tricolor">
               <div id="color" className={"color-"+this.state.color} onClick={this.cambioColor}></div>
            </div>
        );
    }
}