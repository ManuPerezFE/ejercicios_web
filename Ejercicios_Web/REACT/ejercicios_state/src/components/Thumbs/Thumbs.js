import React, {Component} from 'react';
import Up from './img/up.png';
import Down from './img//down.png';

export default class Thumbs extends Component{

    constructor(props){
        super(props);
        this.state = { 
            cambio: Up
        };
        this.cambioIMG = this.cambioIMG.bind(this);
    }

    cambioIMG(){
        if (this.state.cambio == Up )
            this.setState({
                cambio: Down
            })
        else{
            this.setState({
                cambio: Up
            }) 
        }   
    }

    render(){
        return(
            <div id="thumbs">
               <img className="thumbs" onClick={this.cambioIMG} src={this.state.cambio} alt="Imagen" />
            </div>
        );
    }
}