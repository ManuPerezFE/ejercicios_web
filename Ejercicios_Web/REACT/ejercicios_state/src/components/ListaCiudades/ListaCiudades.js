import React, {Component} from 'react';
import {Ciudades} from './src/Datos.json';

export default class ListaCiudades extends Component{

    constructor(props){
        super(props);
        this.state = { 
            ciudad: ""
        };
        this.getDatos = this.getDatos.bind(this);
        this.comprueba = this.comprueba.bind(this);
        this.pulsa = this.pulsa.bind(this);
    }
    //OBTENGO LO QUE ME ESCRIBEN EN EL INPUT
    getDatos = e =>{
        this.setState({ciudad: e.target.value})
    }
    //DEVUELVO SI LO QUE ESCRIBO EN EL INPUT ESTA EN EL ARRAY (TRUE-FALSE)
    comprueba = city =>{
        let ciudad = this.state.ciudad;
        let strl = ciudad.length;
        let compar = city.ciudad;
        return (ciudad.toLowerCase() == compar.toLowerCase().substr(0, strl));
    }

    pulsa = dato => {
        this.setState({ciudad: dato});
    }
    render(){
        let sele = Ciudades.filter(this.comprueba);//FILTRO
        //MONTO TODAS LAS CIUDADES
        let salida = sele.map((item, i) => {
            return <div key={i} onClick={ e => this.pulsa(item.ciudad)}>{item.ciudad} <br/></div>
        });
        return(
            <div id="listaCiudades">
                <input value={this.state.ciudad} onChange={e => {this.getDatos(e)}} placeholder="Escribe una ciudad" />
                <br />
                {salida}
            </div>
        );
    }
}