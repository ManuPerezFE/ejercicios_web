import React, { Component } from 'react';

export default class ListaCiudadesFetch extends Component {

    constructor(props) {
        super(props);
        this.state = {
            opProvin: "",
            provin: 1,
            lPobla: []
        };
        this.url = "http://kiosko.gestionproyectos.com/?controller=API&";
        this.urlProvin = this.url + 'action=listadoProvin';
        this.urlPobla = this.url + 'action=listadoPoblaW&provin=';
    }

    //cuando carga el componente, consigo las provincias
    componentDidMount(){
        fetch(this.urlProvin, {
            method: 'GET'
        })
        .then(result => result.json())
        .then(result => {
            let opProvin = result.Cp_provincias.map((item) =>
                <option key={item.cppro_id} value={item.cppro_id}>{item.cppro_nombre}</option>
            )
            this.setState({opProvin: opProvin});
        })
    }
    //tengo el ID de la provincia seleccionada, y consigo las ciudades de esa provincia con otro fetch
    cambioProvin = e =>{
        let valor = e.target.value;
        this.setState({provin: valor});
        fetch(this.urlPobla + valor, {
            method: 'GET'
        })
        .then(result => result.json())
        .then(result => {
            this.setState({lPobla: result.Cp_poblacion});
        })
    }

    render() {
        //construyo las ciudades de la provincia seleccionada
        let opPobla = this.state.lPobla.map((item) =>
            <option key={item.cppob_id} value={item.cppob_id}>{item.cppob_nombre}</option>
        )
        return (
            <div id="listaCombo">
                <select onChange={this.cambioProvin}>
                    {this.state.opProvin}
                </select>
                <select>
                    {opPobla}
                </select>
            </div>
        );
    }
}