import React, { Component } from 'react';
import { Ciudades } from './src/Datos.json';

export default class ListaCiudades extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ciudad: "",
            comarca: ""
        };
        this.getDatos = this.getDatos.bind(this);
        this.cambiarOpcion = this.cambiarOpcion.bind(this);
    }

    getDatos = e => {
        this.setState({ ciudad: e.target.value });
    }

    cambiarOpcion = e => {
        this.setState({ comarca: e.target.value });
    }

    render() {
        var hash = {};
        let comarcasSinRepetir = Ciudades.filter(function(current) {
            var exists = !hash[current.comarca] || false;
            hash[current.comarca] = true;
            return exists;
        });
        let comarcas = comarcasSinRepetir.map((item, i) => <option key={i} value={item.comarca}>{item.comarca}</option>);
        let ciudades = Ciudades.map((item, i) => {
            if(item.comarca === this.state.comarca){
                return <option key={i} value={item.ciudad}>{item.ciudad}</option>;
            }
        });
        
        return (
            <div id="listaCombo">
                <select value={this.state.comarca} onChange={this.cambiarOpcion}>
                    {comarcas}
                </select>
                <select value={this.state.comarca} onChange={this.getDatos}>
                    {ciudades}
                </select>
            </div>
        );
    }
}