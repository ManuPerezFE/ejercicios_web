import React from 'react';

const InformacionClima = props => {
    let urlIcon = "http://openweathermap.org/img/w/" + props.datos.icono + ".png";
    return(
        <div className="temp"> 
            {props.datos.temp_max ?
                <div className="img">
                    <img id="widcon" src={urlIcon} alt="Weather icon" />
                    <p>{props.datos.temp_max}&deg;C, {props.datos.temp_min}&deg;C</p>
                </div>
                :
                <div></div>
            }
        </div>
    )
}

export default InformacionClima;