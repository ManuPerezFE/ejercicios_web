import React from 'react';

const FormularioClima = props =>(
    <div className="card car-body">
        <form onSubmit={props.origen}>
            <div className="form-group">
                <input type="text" name="city" placeholder="Nombre de la cuidad" className="form-control" />
            </div>
            <div className="form-group">
                <input type="text" name="country" placeholder="Nombre del país" className="form-control" />
                <button className="btn btn-success btn-block">
                    Pulse para obtener información
                </button>
            </div>
        </form>
    </div>
)


export default FormularioClima;