import React, {Component} from 'react';

import InformacionClima from './components/InformacionClima';
import FormularioClima from './components/FormularioClima';
import './css/Tiempo.css';

import {WEATHER_APIKEY} from './keys';

export default class Tiempo extends Component{

    constructor(props){
        super(props);
        this.state = { 
            datos: []
        };
    }
    getCoordenadas(){
        //consigo la geolocalizacion
        return new Promise(function(resolve, reject){
            navigator.geolocation.getCurrentPosition(resolve, reject);
        });
    }
    getDatos = async e => {
        e.preventDefault();
        const {city, country} = e.target.elements;
        const cityValue = city.value;
        const countryValue = country.value;
        
        let WEATHER_FUNCION = "forecast";
        let WEATHER_APIURL, respuesta, datos, long, lat;

        if(cityValue == '' && countryValue == ''){
            if(navigator.geolocation){
                const pos = await this.getCoordenadas();
                long = pos.coords.longitude;
                lat = pos.coords.latitude;
                console.log(pos);
            }
            //await this.getCoordenadas();
            WEATHER_APIURL = `http://api.openweathermap.org/data/2.5/${WEATHER_FUNCION}?lat=${lat}&lon=${long}&APPID=${WEATHER_APIKEY}&units=metric`;
        }else{
            WEATHER_APIURL = `http://api.openweathermap.org/data/2.5/${WEATHER_FUNCION}?q=${cityValue},
                ${countryValue}&APPID=${WEATHER_APIKEY}&units=metric`;
        }

        respuesta = await fetch(WEATHER_APIURL);
        datos = await respuesta.json();
        console.log(datos);
        //me guardo la fecha inicial
        let aux=[];//me creo array auxiliar para guardarme los datos
        let dia={
            'temp_max': 0,
            'temp_min': 0,
            'icono': '',
            'fecha': null
        };
        datos.list.forEach(function(item, i){
            let fecha = item.dt_txt.substr(0,10);
            if(dia.fecha != fecha){
                if(dia.fecha != null){
                    aux.push(dia);
                }
                dia={
                    'temp_max': item.main.temp_max,
                    'temp_min': item.main.temp_min,
                    'icono': item.weather[0].icon,
                    'fecha': item.dt_txt.substr(0,10)
                }
            }else{
                if(dia.temp_max < item.main.temp_max){
                    dia.temp_max = item.main.temp_max;
                }
                if(dia.temp_min > item.main.temp_min){
                    dia.temp_min = item.main.temp_min;
                }
            }
        });
        aux.push(dia);
        this.setState({datos: aux});
    }

    render(){
        let datos = this.state.datos;
        let mostrar = datos.map((item, i) => (<InformacionClima key={i} datos={item}/>))
        return(
            <div className="container p-4">
                <div className="row">
                    <div className="col-md-6">
                        <FormularioClima origen={this.getDatos} />
                    </div>
                    <div id="temperaturas">
                        {mostrar}
                    </div>
                </div>
            </div>
        )
    }
}