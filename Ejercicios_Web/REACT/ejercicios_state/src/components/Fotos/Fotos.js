import React, {Component} from 'react';
import Bici from './img/bici.jpg';
import Coche from './img/coche.jpg';
import Bus from './img/bus.jpg';
import Moto from './img/moto.jpg';

export default class Fotos extends Component{

    constructor(props){
        super(props);
        this.state = { 
            opcion: Bici
        };
        this.cambiarOpcion = this.cambiarOpcion.bind(this);
    }
    cambiarOpcion = e => {
        this.setState({opcion: e.target.value})
    }
    
    render(){
        return(
            <div id="fotos">
               <select value={this.state.opcion} onChange={this.cambiarOpcion}>
                    <option value={Coche}>Coche</option>
                    <option value={Moto}>Moto</option>
                    <option value={Bici}>Bici</option>
                    <option value={Bus}>Bus</option>
                </select>
                <br />
               <img src={this.state.opcion} alt={this.state.opcion} />
            </div>
        );
    }
}