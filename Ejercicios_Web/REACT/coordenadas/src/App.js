import React, {Component} from 'react';
import './App.css';
import Coordenadas from './Coordenadas';
import Nombres from './Nombres';


class App extends Component {

  constructor(props){
    super(props);
    this.state = { 
        mostrarCoordenadas: false,
        mostrarNombres: false
    };
    this.mostrarC = this.mostrarC.bind(this);
    this.mostrarN = this.mostrarN.bind(this);
  }

  mostrarC(){
    this.setState({
      mostrarCoordenadas: !this.state.mostrarCoordenadas//pone lo contrario de lo qe tengo(true-false)
    })
  }
  mostrarN(){
    this.setState({
      mostrarNombres: !this.state.mostrarNombres//pone lo contrario de lo qe tengo(true-false)

    })
  }
  render(){
    return(
      <div className="App">
        <button onClick={this.mostrarC} className="btn btn-success btn-block">Mostrar Coordenadas</button>
        <button onClick={this.mostrarN} className="btn btn-success btn-block">Mostrar Nombres</button>

        {this.state.mostrarCoordenadas ? <div className="coor"><Coordenadas /></div> : null}
        {this.state.mostrarNombres ? <div className="name"><Nombres /></div> : null}
      </div>
    )
  }
}

export default App;
