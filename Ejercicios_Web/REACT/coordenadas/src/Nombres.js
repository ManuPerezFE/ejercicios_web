import React, {Component} from 'react';

class Nombre extends Component {
    constructor(props){
        super(props);
        this.state = { 
            name: null,
            mostrar: false
        };
        this.getNombre = this.getNombre.bind(this);
        this.mostrar = this.mostrar.bind(this);
        this.limpiar = this.limpiar.bind(this);
    }

    getNombre = e => {
        this.setState({
            name: e.target.value
        })
    }
    mostrar(){
        this.setState({
            mostrar: true
        })
    }
    limpiar(){
        this.setState({
            mostrar: false,
            name: null
        })
    }

    render() {
        return (
            <div>
                {this.state.mostrar ? (
                    <div>
                        <h2>Hola {this.state.name}</h2>
                        <button onClick={this.limpiar} className="btn btn-warning btn-block">
                            Limpiar
                        </button>
                    </div>
                ) : (
                    <div>
                        <input type="text" name="name" placeholder="Escribe tu nombre" className="form-control" onChange={e => {this.getNombre(e)}} />
                        <button onClick={this.mostrar} className="btn btn-warning btn-block">
                            Saludar
                        </button>
                    </div>
                )}
            </div>
        )
    }
}

export default Nombre;
