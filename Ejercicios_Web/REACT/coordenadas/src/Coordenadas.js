import React, {Component} from 'react';

class Coordenadas extends Component {
    constructor(props){
        super(props);
        this.state = { 
            latitud: null,
            longitud: null,
            off: false
        };
        this.getCoordenadas = this.getCoordenadas.bind(this);
        this.salir = this.salir.bind(this);
    }

    getCoordenadas(){
        //consigo la geolocalizacion
        navigator.geolocation.getCurrentPosition(pos => {
            this.setState({
                latitud: pos.coords.latitude,
                longitud: pos.coords.longitude,
                off: false
            })
        });
    }
    salir(){
        this.setState({
            latitud: null,
            longitud: null,
            off: true
        })
    }

    render() {
        return (
            <div>
                {this.state.latitud == null ? (
                    this.state.off ? ( <div><h2>ADIOS</h2></div>) : ( <div><h2>CARGANDO...</h2></div> )
                ) : (
                    <div>
                        <h4>Latitud: {this.state.latitud}</h4>
                        <h4>Longitud: {this.state.longitud}</h4>
                    </div>
                )}
                <button onClick={this.getCoordenadas} className="btn btn-info btn-block">
                    Leer
                </button>
                <button onClick={this.salir} className="btn btn-info btn-block">
                    Cerrar
                </button>
            </div>
        )
    }

    /*
    //SE LLAMA A ESTE METODO CUANDO SE CREA EL OBJETO
    componentDidMount() {
        this.setState({
            latitud: 5,
            longitud: 6
        })
    }
    */
}

export default Coordenadas;
