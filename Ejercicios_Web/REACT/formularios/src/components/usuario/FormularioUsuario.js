import React, {Component} from 'react';

class FormularioUsuario extends Component{
    
    constructor(props){
        super(props);
        this.state = { 
            idUsuario: 0,
            nameUsuario: 'Manu',
            mailUsuario: 'manu@gmail.com'
        };
    }

    getDatos = e => {
       this.setState({[e.target.name] : e.target.value});
       this.props.control(true);
    }
    
    componentDidMount(){
        this.setState({...this.props.usuario});
    }

    render(){
        return(
            <div className="card car-body">
                <form onSubmit={this.props.final}>
                    <div className="form-group">
                        <h2>USUARIO</h2>
                        <label>ID</label>
                        <input value={this.state.idUsuario} onChange={e => {this.getDatos(e)}} type="text" name="idUsuario" placeholder="ID" className="form-control" />
                        <label>Nombre</label>
                        <input value={this.state.nameUsuario} onChange={e => {this.getDatos(e)}} type="text" name="nameUsuario" placeholder="Nombre" className="form-control" />
                        <label>Correo Electronico</label>
                        <input value={this.state.mailUsuario} onChange={e => {this.getDatos(e)}} type="text" name="mailUsuario" placeholder="Correo Electronico" className="form-control" />
                        <button className="btn btn-warning">Enviar</button>
                    </div>
                </form>
            </div>
        )
    }
}


export default FormularioUsuario;