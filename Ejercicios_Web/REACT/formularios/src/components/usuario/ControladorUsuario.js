import React, {Component} from 'react';
import FormularioUsuario from './FormularioUsuario';
import RespuestaFormulario from '../RespuestaFormulario';

class ControladorProducto extends Component{

    constructor(props){
        super(props);
        this.state = { 
            datos: [],
            data: null
        };
    }

    getDatos = e =>{
        e.preventDefault();
        const {idUsuario, nameUsuario, mailUsuario} = e.target.elements;
        const dato = this.state.datos;
        dato.push([idUsuario.value, nameUsuario.value, mailUsuario.value]);
        this.setState({datos: dato});
        this.comunicaciones(dato);
        console.log(dato);
    }

    comunicaciones = datos =>{
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        const url = "https://postman-echo.com/post";
        fetch(proxyurl + url, {
            method: "POST",
            headers: {
                'Content-type': 'application/json',
                'Cache-Control': 'no-cache'
            },
            body: JSON.stringify(datos),
        })
        .then(result => result.json())
        .then(result =>{
            this.setState({data: result});
            this.props.control(false);  
            console.log(this.state.data);
        })
    }

    render(){
        let usuario= {
            idUsuario: 2,
            nameUsuario: 'Usuario 2',
            mailUsuario: 'Mail usuario 2'
        }
        return(
            <div id="FormProd">
                <FormularioUsuario final={this.getDatos} usuario={usuario} control={this.props.control} />
                {this.state.data != null ? <RespuestaFormulario valor={this.state.data} /> : null }
            </div>
        )
    }
}

export default ControladorProducto;