import React, {Component} from 'react';

class FormularioProducto extends Component{

    constructor(props){
        super(props);
        this.state = { 
            idProducto: 0,
            descProducto: 'Categoria1',
            expProducto: 'Explicacion1'
        };
    }

    componentDidMount(){
        this.setState({...this.props.producto});
    }

    getDatos = e => {
       this.setState({[e.target.name] : e.target.value});
       this.props.control(true);
    }

    render(){
        return(
            <div className="card car-body">
                <form onSubmit={this.props.final}>
                    <div className="form-group">
                        <h2>PRODUCTO</h2>
                        <label>Código de producto</label>
                        <input value={this.state.idProducto} onChange={e => {this.getDatos(e)}} type="number" name="idProducto" placeholder="Código de producto" className="form-control" />
                        <label>Descripcion corta</label>
                        <input value={this.state.descProducto} onChange={e => {this.getDatos(e)}} type="text" name="descProducto" placeholder="Descripcion corta" className="form-control" />
                        <label>Explicacion</label>
                        <input value={this.state.expProducto} onChange={e => {this.getDatos(e)}} type="text" name="expProducto" placeholder="Explicacion" className="form-control" />
                        <button className="btn btn-warning">Enviar</button>
                    </div>
                </form>
            </div>
        )
    }
}


export default FormularioProducto;