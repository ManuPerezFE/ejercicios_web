import React, {Component} from 'react';

class FormularioCategoria extends Component {

    constructor(props){
        super(props);
        this.state = { 
            idCategoria: 0,
            nameCategoria: 'Categoria1'
        };
    }

    getDatos = e => {
        /*
        switch (e.target.name){//pilla el NAME del input
            case 'idCategoria':
                this.setState({idCategoria: e.target.value});
                break;
            case 'nameCategoria':
                this.setState({nameCat: e.target.value});
                break;
        }
        */
        this.setState({[e.target.name] : e.target.value})
        this.props.control(true);
    }

    //recibimos el objeto que nos pasa el padre y metemos sus datos en el state
    componentDidMount(){
        this.setState({...this.props.categoria});
    }

    render(){
        return(
            <div className="card car-body">
                <form onSubmit={this.props.final}>{/*llamamos a FINAL qe es del padre*/}
                    <div className="form-group">
                        <h2>CATEGORIA</h2>
                        <label>ID Categoria</label>
                        <input value={this.state.idCategoria} onChange={e => {this.getDatos(e)}} name="idCategoria" type="number" placeholder="ID de la categoria" className="form-control" />
                        <label>Nombre de categoria</label>
                        <input value={this.state.nameCategoria} onChange={e => {this.getDatos(e)}} name="nameCategoria" type="text" placeholder="Nombre de la categoria" className="form-control" />
                        <button className="btn btn-warning">Enviar</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default FormularioCategoria;