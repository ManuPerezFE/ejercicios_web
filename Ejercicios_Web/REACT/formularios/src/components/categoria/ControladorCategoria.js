import React, {Component} from 'react';
import FormularioCategoria from './FormularioCategoria';
import RespuestaFormulario from '../RespuestaFormulario';

class ControladorCategoria extends Component{

    constructor(props){
        super(props);
        this.state = { 
            datos: [],
            data: null
        };
    }

    getDatos = e =>{
        e.preventDefault();
        const {idCategoria, nameCategoria} = e.target.elements;
        const dato = this.state.datos;
        dato.push([idCategoria.value, nameCategoria.value]);
        this.setState({datos: dato});
        this.comunicaciones(dato);
        console.log(dato);
    }

    comunicaciones = datos =>{
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        const url = "https://postman-echo.com/post";
        fetch(proxyurl + url, {
            method: "POST",
            headers: {
                'Content-type': 'application/json',
                'Cache-Control': 'no-cache'
            },
            body: JSON.stringify(datos),
        })
        .then(result => result.json())
        .then(result =>{
            this.setState({data: result})
            this.props.control(false);
            console.log(this.state.data)
        })
    }

    render(){
        //creamos una categoria y se lo pasamos al formulario para que lo carge 
        let categoria = {
            idCategoria: 2,
            nameCategoria: 'Categoria 2'
        }
        
        return(
            <div id="FormCat">
                {/*categoria={categoria}  le pasamos el objeto creado al formulario*/}
                <FormularioCategoria final={this.getDatos} categoria={categoria} control={this.props.control} />
                {this.state.data != null ? <RespuestaFormulario valor={this.state.data} /> : null }
            </div>
        )
    }
}

export default ControladorCategoria;