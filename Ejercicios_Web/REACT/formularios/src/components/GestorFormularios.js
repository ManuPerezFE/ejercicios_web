import React, {Component} from 'react';

import Categorias from './categoria/ControladorCategoria';
import Productos from './producto/ControladorProducto';
import Usuarios from './usuario/ControladorUsuario';

class GestorFormulario extends Component {

  constructor(props){
    super(props);
    this.state = { 
        verCategorias: false,
        verProductos: false,
        verUsuarios: false,
        modificacion: false
    };
  }

  activaMod = (estado) => this.setState({modificacion: estado});
  
  componentDidMount(){
    this.setState({modificacion: false});
  }
  
  //controlo que formulario se activa
  botonera = e =>{
    if((this.state.modificacion &&
      window.confirm("Cambios en el formulario.\n¿Quiere cambiar?")) ||
    !this.state.modificacion){
      this.setState({
        verCategorias: false,
        verProductos: false,
        verUsuarios: false,
        modificacion: false
      });
      this.setState({[e.target.name] : true});
    }
  }
  
  render(){
    return(
      <div id="botones">
        <button name="verCategorias" onClick={this.botonera} className="btn btn-success">Mostrar Categorias</button>
        <button name="verProductos" onClick={this.botonera} className="btn btn-success">Mostrar Productos</button>
        <button name="verUsuarios" onClick={this.botonera} className="btn btn-success">Mostrar Usuarios</button>

        {this.state.verCategorias ? <div id="categorias"><Categorias control={this.activaMod} /></div> : null}
        {this.state.verProductos ? <div id="productos"><Productos control={this.activaMod} /></div> : null}
        {this.state.verUsuarios ? <div id="usuarios"><Usuarios control={this.activaMod} /></div> : null}
      </div>
    )
  }
}

export default GestorFormulario;