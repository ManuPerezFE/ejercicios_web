import React, {Component} from 'react';
import './App.css';

import GestorFormulario from './components/GestorFormularios';

class App extends Component {

  render(){
    return(
      <div id="App">
        <GestorFormulario />
      </div>
    )
  }
}

export default App;
