import React, {Component} from 'react';
import './App.css';
import Nombre from './Nombre';


class App extends Component {

  render(){
    return(
      <div className="App">
        <Nombre />
      </div>
    )
  }
}

export default App;
